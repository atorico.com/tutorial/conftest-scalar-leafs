package main

import data.stage

deny[msg]{
    [ path, value ] := walk(input[_])
    is_scalar( value )
    needle := data.stage.should_not_contain[_]
    contains_text(value, needle)
    msg := sprintf("Value %s: %s", [ value, needle.name ])
}

contains_text( val, needle ){
    regex.match(needle.regex, val)
}

contains_text( val, needle ){
    contains(val, needle.text)
}

is_scalar( val ){
    not is_object( val )
    not is_collection( val )
}

is_object( val ){
    sVal := sprintf("%s", [ val ])
    startswith(sVal, "{" )
    endswith(sVal, "}" )
}

is_collection( val ){
    sVal := sprintf("%s", [ val ])
    startswith(sVal, "[" )
    endswith(sVal, "]" )
}
